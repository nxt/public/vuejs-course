import { shallowMount } from "@vue/test-utils";
import HelloWorld from "@/components/HelloWorld.vue";

// https://vue-test-utils.vuejs.org/api/#mount
// https://vue-test-utils.vuejs.org/api/#shallowmount
// https://vue-test-utils.vuejs.org/api/wrapper

describe("HelloWorld.vue", () => {
    it("renders props.msg when passed", () => {
        const wrapper = shallowMount(HelloWorld);

        let msg = "new message";
        wrapper.setProps({ msg });
        expect(wrapper.find("h1").text()).toMatch(msg);

        msg = "newest message";
        wrapper.setProps({ msg });
        expect(wrapper.find("h1").text()).toMatch(msg);
    });
});
