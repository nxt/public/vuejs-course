import { shallowMount, mount } from "@vue/test-utils";
import App from "@/App.vue";

// https://vue-test-utils.vuejs.org/api/#mount
// https://vue-test-utils.vuejs.org/api/#shallowmount
// https://vue-test-utils.vuejs.org/api/wrapper

describe("App.vue", () => {
    it("render App only", () => {
        const wrapper = shallowMount(App);
        expect(wrapper.find("h1").text()).toMatch("Hello to the new App");
        expect(wrapper.findAll("h1").length).toBe(1);
    });

    it("render App with child components", () => {
        const wrapper = mount(App);
        expect(wrapper.find("h1").text()).toMatch("Hello to the new App");
        expect(wrapper.findAll("h1").length).toBe(2);
    });
});
