// https://docs.cypress.io/api/introduction/api.html

describe("My First Test", () => {
    it("Visits the app root url", () => {
        cy.visit("/");
        cy.contains("div#app h1", "Hello to the new App");
        cy.contains(".hello h1", "Welcome to Your Vue.js App");
    });
});
