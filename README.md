# Vue.js

## Build Locally

Requires Docker!

```bash
docker build -t my_pandoc:snapshot 00_Slides # Just once, or after changes to Dockerfile
cd 00_Slides
./build.sh
```
