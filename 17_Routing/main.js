import Vue from "vue";
import Router from "vue-router";
import App from "./App";

Vue.use(Router);

Vue.prototype.$loginData = {};

new Vue({
    render: h => h(App)
}).$mount("#app");
