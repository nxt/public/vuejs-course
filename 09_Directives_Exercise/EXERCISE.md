# Übung Komponenten Teil 2

#### 0. Führe einen `npm install` und `npm run serve` im `exercise/slideshow` Ordner aus.

#### 1. Führe in der `Header` Komponente einen Subtitel ein. Dieser soll aus einem Data-Property abgefüllt werden, das HTML beinhalten kann. (Zum Beispiel einen blauen Subtitel).

#### 2. Zeige nur Bilder an mit einer HTTPS-URL.