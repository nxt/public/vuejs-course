# Übung Komponenten Teil 1

#### 0. Führe einen `npm install` und `npm run serve` im `exercise/slideshow` Ordner aus.

#### 1. Bennene die bestehende Komponente HelloWorld in Slideshow um.

#### 2. Entferne in der Slideshow Komponente den Inhalt (template, script und style sein lassen).

#### 3. Entferne das Logo aus der App Komponente.

#### 4. Erstelle eine neue Komponente namens `Header` welche im Verzeichnis `src/components` liegt.

Tipp: CTRL + Space im neuen File um die Grundstruktur zu erstellen (scaffold)

#### 5. Füge die Komponente `Header` in der `Slideshow` Komponente ein.

#### 6. Füge den Titel "Slideshow" als Variable in die Komponente `Header` ein und gebe sie im Template aus.

#### 7. Definiere eine applikationsübergreifende Schriftart, welche dir gefällt.

#### 8. Zentriere den Titel in der `Header` Komponente.

#### 9. Erstelle eine neue Komponente namens `SlideshowImage` welche im Verzeichnis `src/components` liegt.

#### 10. Füge die Komponente `SlideshowImage` in der `Slideshow` Komponente ein.

#### 11. Füge ein Bild in die `SlideshowImage` Komponente ein. Die URL des Bildes sollte aus einer Variable kommen.
Tipp: [https://unsplash.com]()

#### 12. Zentriere das Bild und füge einen `box-shadow` hinzu.
Tipp: [https://www.cssmatic.com/box-shadow]()
