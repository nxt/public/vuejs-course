import Vue from "vue";
import Vuex from "vuex";
import App from "./App";

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        value: ""
    },
    mutations: {
        value(state, value) {
            state.value = value;
        }
    },
    getters: {
        value: state => state.value
    }
});

new Vue({
    store,
    render: h => h(App)
}).$mount("#app");
