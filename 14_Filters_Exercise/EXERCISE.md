# Übung Filters

#### 0. Führe einen `npm install` und `npm run serve` im `exercise/slideshow` Ordner aus.

#### 1. Verwende als Subtitel wieder einen normalen Text, kein HTML.

#### 2. Erstelle einen Filter, der dazu führt, dass der erste Buchstabe des Subtitels gross geschrieben wird.

