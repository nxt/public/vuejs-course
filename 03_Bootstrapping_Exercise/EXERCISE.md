# Übung Bootstrapping

#### 1. Erstelle eine Hello-World Applikation mit Hilfe des globalen CLI Services.

* ```npm install -g @vue/cli-service-global```
* Öffne ein Terminal und navigiere in den Ordner 03_Bootstrapping_Exercise/prototyping
* Erstelle ein File namens app.vue: ```<template><div>hello world</div></template>```
* Starte die Applikation: ```vue serve app.vue```
* Öffne http://localhost:8080 in deinem Browser.

#### 2. Generiere mit Hilfe des GUIs der Vue CLI eine neue Vue.js Applikation im Verzeichnis `cli-ui` mit den folgenden Eigenschaften:
* Name: slideshow
* Package manager: default
* Git repository: disabled
* Preset: Default preset (babel, eslint)

Tipp: https://cli.vuejs.org/guide/creating-a-project.html#using-the-gui

#### 3. Generiere mit der Vue CLI (ohne GUI) eine neue Vue.js Applikation im Verzeichnis `cli` mit den folgenden Eigenschaften:
* Name: slideshow
* Package manager: default
* Git repository: disabled
* Preset: Default preset (babel, eslint)


#### 4. Starte die Vue Applikation welche nicht über das GUI generiert wurde


#### 5. Führe einen produktiven Build der Applikation aus. 
Tipps:
* [https://cli.vuejs.org/guide/mode-and-env.html#modes]()
* Blick ins package.json
