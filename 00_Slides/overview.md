# Vue.js Übersicht

## Web Frameworks

![](react.png "React Logo"){ width=33% }
![](vuejs.png "Vue.js Logo"){ width=33% }
![](angular.png "Angular Logo"){ width=33% }

## Facts

- Erscheinungsjahr: 2014
- Entwickler: Evan You
- Aktuelle Version: 2.6.10
- Nächste Major Version: 3.0.0 - 4. Quartal 2019
- Programiersprache: JavaScript oder TypeScript

## Visual Studio Code - Extensions

-   Vetur
-   ESLint
-   Prettier
-   EditorConfig for VS Code

## Visual Studio Code - Settings

```javascript
{
    "files.autoSave": "afterDelay",
    "eslint.validate": [
        {
          "language": "vue",
          "autoFix": true
        },
        {
          "language": "javascript",
          "autoFix": true
        }
      ],
    "eslint.autoFixOnSave": true,
    "prettier.eslintIntegration": true
}
```

## NPM

```
npm install -g @vue/cli @vue/cli-service-global
npm install
```

## Run Examples

```
vue serve 02_Templates/App.vue 
vue serve 17_Routing/main.js
```

