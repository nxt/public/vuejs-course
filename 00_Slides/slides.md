---
title: Vue.js
author:
  - Mirco Widmer
  - Michael Gerber
institute:
  - nxt Engineering GmbH
date: 2019-06-13

# html slides
lang: de-CH
revealjs-url: https://revealjs.com

# latex/pdf slides
fontfamily: roboto
header-includes:
- |
  ```{=latex}
  \definecolor{main}{RGB}{0, 61, 113}
  \definecolor{background}{RGB}{236, 240, 241}
  ```
---

!include intro.md

!include overview.md

!include contact.md
