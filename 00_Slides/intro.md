# Einführung

## Wer sind wir?

```{=latex}
\begin{scriptsize}
```

```typescript
const michi = new Teacher({
   name: 'Michael Gerber',
   telephone: '+79 440 23 02',
   email: 'michael.gerber@nxt.engineering',
   education: [{
       school: 'HSR Hochschule für Technik Rapperswil',
       degree: 'Bachelor of Science'
   }, {
       school: 'University of Oxford',
       degree: 'Master of Science'
   }],
   jobs: [{
       company: 'Swiss Re',
       job: 'Junior Software Engineer'
   }, {
       company: 'HRM Systems AG',
       job: 'Senior Software Engineer / Team Leader'
   }, {
       company: 'nxt Engineering GmbH',
       job: 'Managing Director \& Senior Software Engineer'
   }]
});
```

```{=latex}
\end{scriptsize}
```


## Wer sind wir?

```{=latex}
\begin{scriptsize}
```

```typescript
const mirco = new Teacher({
   name: 'Mirco Widmer',
   telephone: '+77 964 08 05',
   email: 'mirco.widmer@nxt.engineering',
   education: [{
       school: 'HSR Hochschule für Technik Rapperswil',
       degree: 'Bachelor of Science'
   }],
   jobs: [{
       company: 'Swiss Life',
       job: 'Software Engineer'
   }, {
       company: 'Ergon Informatik AG',
       job: 'Senior Software Engineer'
   }, {
       company: 'nxt Engineering GmbH',
       job: 'Managing Director \& Senior Software Engineer'
   }]
});
```

```{=latex}
\end{scriptsize}
```

## Was macht nxt?

```{=latex}
\begin{scriptsize}
```

```typescript
const nxt = new Company({
   name: 'nxt Engineering GmbH',
   telephone: '+41 44 508 52 45',
   website: 'https://nxt.engineering',
   email: 'hallo@nxt.engineering',
   founders: [christian, mirco, michi]
});
const customer = new Customer("Firmenname");
const project = customer.project("Komplexe Webapplikation");
project.iterations().forEach(iteration => {
   // Gemeinsam planen wir die nächsten Schritte.
   nxt.plan(iteration);
   // Ihre Webapplikation wird durch uns gebaut.
   nxt.build(iteration);
   // Wir sorgen für einen reibungslosen Betrieb.
   nxt.run(iteration);
});
```

```{=latex}
\end{scriptsize}
```

## Wer bist du?

```{=latex}
\begin{scriptsize}
```

```typescript
enum Experience {
   VeryGood,
   Good,
   Ok,
   Bad,
}

interface Student {
   getName(): string;

   getCompany(): string;
   
   getKnowledge(): {
       java: Experience,
       dotNet: Experience,
       javaScript: Experience,
       html: Experience
   };
}
```

```{=latex}
\end{scriptsize}
```

## Timeline Tag 1

### Vormittag

- Vue.js Übersicht
- Komponenten
- Templates 
- Methoden 

### Nachmittag

- Properties
- Direktiven
- Filters

## Timeline Tag 2

### Vormittag

- Slots
- Mixins
- Routing

### Nachmittag

- Http
- Testing

