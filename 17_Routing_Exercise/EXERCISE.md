# Übung Routing

#### 0. Führe einen `npm install` und `npm run serve` im `exercise/slideshow` Ordner aus.

#### 1. Erstelle im `src/components` Verzeichnis eine neue Komponente mit dem Namen `Fullscreen`.

#### 2. Erweitere die Applikation um Routing analog dem Theoriebeispiel:
* `/fullscreen` soll die `fullscreen` Komponente anzeigen.
* `/slideshow` soll die `slideshow` Komponente anzeigen.
* `/` soll ebenfalls die `slideshow` Komponente anzeigen.

Tipp: Passe die Files App.vue und main.js an.

#### 3. Füge der `Slideshow` Komponente einen Button hinzu, welcher zur `Fullscreen` Komponente navigiert. Als Query Parameter soll die URL des aktuellen Bildes mitgegeben werden. 

Tipp: [https://dev.to/napoleon039/the-lesser-known-amazing-things-vuerouter-can-do-25di]()

#### 4. Füge einen globalen Style für den Button hinzu.

#### 5. Füge die `SlideshowImage` Komponente der `Fullscreen` Komponente hinzu.
Tipp: Erstelle ein Data Property (data() { return { ... }}) `url` und setze dies in einer `mounted()` Funktion. Verwende dieses Data Property für die `SlideshowImage` Komponente.

#### 6. Füge der `Fullscreen` Komponente einen Button hinzu, um auf die Slideshow zurück zu navigieren.
