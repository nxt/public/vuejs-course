# Übung Komponenten Teil 2

#### 0. Führe einen `npm install` und `npm run serve` im `exercise/slideshow` Ordner aus.

#### 1. Definiere die URL in der `SlideshowImage` Komponente als Props-Property anstatt als Data-Property. Die URL soll als Variable in der `Slideshow` Komponente definiert werden.

#### 2. Wenn auf das Bild in der `SlideshowImage` Komponente geklickt wird, soll die `Slideshow` Komponente benachrichtigt werden und die URL des Bildes ändern.
